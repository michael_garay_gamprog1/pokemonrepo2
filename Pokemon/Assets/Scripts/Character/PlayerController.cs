using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] string name;
    [SerializeField] Sprite sprite;

    const float playerOffsetY = 0.3f;

    public event Action OnEncountered;
    public event Action<Collider2D> OnEnterTrainerView;

    private Vector2 input;

    private CharacterAnimator animator;
    private Character character;


    private void Awake()
    {
        character = GetComponent<Character>();
    }


    public void HandleUpdate()
    {
        if(!character.isMoving)
        {
            input.x = Input.GetAxisRaw("Horizontal");
            input.y = Input.GetAxisRaw("Vertical");

            if (input.x != 0) input.y = 0;

            if (input != Vector2.zero)
            {
               StartCoroutine(character.Move(input, OnMoveOver));

            }
        }

        character.HandleUpdate();

        if(Input.GetKeyDown(KeyCode.Z))
        {
            Interact();
        }
    }

    void Interact()
    {
        var faceDirection = new Vector3(character.Animator.MoveX, character.Animator.MoveY);
        var interactPosition = transform.position + faceDirection;

        var collider = Physics2D.OverlapCircle(interactPosition, 0.3f, GameLayers.i.InteractableLayer);
        if(collider != null)
        {
            collider.GetComponent<Interactable>()?.Interact(transform);
        }

    }

    private void OnMoveOver()
    {
        CheckForEncounters();
        CheckIfInTrainersView();
    }

    private void CheckForEncounters()
    {
        if(Physics2D.OverlapCircle(transform.position - new Vector3(0,playerOffsetY), 0.2f, GameLayers.i.LongGrass) != null)
        {
            if(UnityEngine.Random.Range(1,101) <= 10)
            {
                character.Animator.isMoving = false;
                OnEncountered();
            }
        }
    }

    private void CheckIfInTrainersView()
    {
        var collider = Physics2D.OverlapCircle(transform.position - new Vector3(0, playerOffsetY), 0.2f, GameLayers.i.fieldOfView);

        if (collider != null)
        {
            character.Animator.isMoving = false;
            OnEnterTrainerView?.Invoke(collider);
        }
    }

    public string Name
    {
        get => name;
    }

    public Sprite Sprite
    {
        get => sprite;
    }
}
