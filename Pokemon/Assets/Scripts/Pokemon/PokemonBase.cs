using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Pokemon", menuName = "Pokemon/Create new pokemon")]
public class PokemonBase : ScriptableObject
{
    [SerializeField] string name;

    [TextArea]
    [SerializeField] string description;

    [SerializeField]  Sprite frontSprite;
    [SerializeField]  Sprite backSprite;

    [SerializeField] PokemonType type1;
    [SerializeField] PokemonType type2;

    //stats
    [SerializeField]  int maxHP;
    [SerializeField]  int attack;
    [SerializeField]  int defense;
    [SerializeField]  int spAttack;
    [SerializeField]  int spDefense;
    [SerializeField]  int speed;

    [SerializeField]  int expYield;
    [SerializeField] GrowthRate growthRate;

    [SerializeField] int catchRate = 255;

    [SerializeField] List<LearnableMove> learnableMoves;

    public static int MaxNumofMoves { get; set; } = 4;

    public int GetExpForLevel(int level)
    {
        if(growthRate == GrowthRate.Fast)
        {
            return 4*(level * level * level)/5;
        }
        else if(growthRate == GrowthRate.MediumFast)
        {
            return level * level * level;
        }
        return -1;
    }

    public string Name
    {
        get { return name; }
    }

    public string Description
    {
        get { return description; }
    }

    public Sprite Front
    {
        get { return frontSprite; }
    }

    public Sprite Back
    {
        get { return backSprite; }
    }

    public PokemonType Type1
    {
        get { return type1; }
    }

    public PokemonType Type2
    {
        get { return type2; }
    }

    public int MaxHP
    {
        get { return maxHP; }
    }

    public int Attack
    {
        get { return attack; }
    }

    public int Defense
    {
        get { return defense; }
    }

    public int SpAttack
    {
        get { return spAttack; }
    }

    public int  SpDefense
    {
        get { return spDefense; }
    }

    public int Speed
    {
        get { return speed; }
    }

    public List<LearnableMove> LearnableMoves
    {
        get { return learnableMoves; }
    }

    public int CatchRate => catchRate;

    public int ExpYield => expYield;

    public GrowthRate GrowthRate => growthRate;
}

[System.Serializable]
public class LearnableMove
{
    [SerializeField] MoveBase moveBase;
    [SerializeField] int level;

    public MoveBase Base
    {
        get { return moveBase; }
    }

    public int Level
    {
        get { return level; }
    }
}


public enum PokemonType
{
    None,
    Normal,
    Fire,
    Water,
    Electric,
    Grass,
    Poison,
    Ice,
    Fighting,
    Ground,
    Flying,
    Psychic,
    Bug,
    Rock,
    Steel,
    Ghost,
    Dragon
}

public enum GrowthRate
{
    Fast, MediumFast
}

public enum Stat
{
    Attack,
    Defense,
    SpAttack,
    SpDefense, 
    Speed
}


public class TypeChart
{
    static float[][] chart =
    {    
        //                  nor  fir  wat    ele   grs   poi   
        /*Nor*/new float[] {1f,  1f,   1f,    1f,  1f,   1f},
        /*Fir*/new float[] {1f, 0.5f, 0.5f,   1f,  2f,   1f},
        /*Wat*/new float[] {1f,  2f,  0.5f,   2f,  0.5f, 1f},
        /*Ele*/new float[] {1f,  1f,   1f,   0.5f, 0.5f, 1f},
        /*Grs*/new float[] {1f, 0.5f, 0.5f,   2f,  0.5f, 0.5f},
        /*Poi*/new float[] {1f,  2f,  0.5f,   1f,  2f,   1f}

    };

    public static float GetEffectiveness(PokemonType attackType, PokemonType defenseType)
    {
        if (attackType == PokemonType.None || defenseType == PokemonType.None)
            return 1;

        int row = (int)attackType - 1;
        int col = (int)defenseType - 1;
        return chart[row][col];
        Debug.Log(chart[row][col]);
    }
}


