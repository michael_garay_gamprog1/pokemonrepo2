using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PartyScreen : MonoBehaviour
{
   [SerializeField] Text messageText;

    PartyMember[] memberSlots;
    List<Pokemon> Pokemons;
    PokemonParty party;

    int selection = 0;

    public Pokemon SelectedMember => Pokemons[selection];

    public void Init()
    {
        memberSlots = GetComponentsInChildren<PartyMember>(true);

        party = PokemonParty.GetPlayerParty();
        SetPartyData();

        party.OnUpdated += SetPartyData;
    }

    public void SetPartyData()
    {
        Pokemons = party.Pokemons;

        for(int i = 0; i < memberSlots.Length; i++)
        {
            if (i < Pokemons.Count)
            {
                memberSlots[i].gameObject.SetActive(true);
                memberSlots[i].SetData(Pokemons[i]);
            }
            else
                memberSlots[i].gameObject.SetActive(false);
        }

        UpdateMemberSelection(selection);

        messageText.text = "Choose a pokemon";

    }

    public void UpdateMemberSelection(int selectedMember)
    {
        for(int i=0; i < Pokemons.Count; i++)
        {
            if (i == selectedMember)
            {
                memberSlots[i].SetSelected(true);
            }
            else
                memberSlots[i].SetSelected(false);
        }
    }

    public void HandleUpdate(Action onSelected, Action onBack)
    {
        var prevSelection = selection;

        if (Input.GetKeyDown(KeyCode.RightArrow))
            ++selection;
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
            --selection;
        else if (Input.GetKeyDown(KeyCode.DownArrow))
            selection += 2;
        else if (Input.GetKeyDown(KeyCode.UpArrow))
            selection -= 2;

        selection = Mathf.Clamp(selection, 0, Pokemons.Count - 1);

        if(selection != prevSelection)
           UpdateMemberSelection(selection);


        if (Input.GetKeyDown(KeyCode.Z))
        {
            onSelected?.Invoke();
        }
        else if (Input.GetKeyDown(KeyCode.X))
        {
            onBack?.Invoke();
        }
    }

    public void SetMessageText(string message)
    {
        messageText.text = message;
    }
}
