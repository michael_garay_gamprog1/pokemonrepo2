using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCController : MonoBehaviour , Interactable
{

    [SerializeField] Dialogue dialogue;
    [SerializeField] List<Vector2> movementPattern;
    [SerializeField] float TimeBetweenPattern;


    NPCState state;

    float idleTimer = 0f;

    int currentMovementPattern = 0;

    Character character;

    private void Awake()
    {
        character = GetComponent<Character>();
    }

   public void Interact(Transform initiator)
    {
        if(state == NPCState.Idle)
        {
            state = NPCState.dialogue;
            character.LookTowards(initiator.position);
            StartCoroutine(DialogueManager.Instance.ShowDialogue(dialogue, () => { idleTimer = 0f; state = NPCState.Idle; }));
        }
    }

    public void Update()
    {

        if(state == NPCState.Idle)
        {
            idleTimer += Time.deltaTime;
            if(idleTimer > TimeBetweenPattern)
            {
                idleTimer = 0f;

                if(movementPattern.Count > 0)
                StartCoroutine(Walk());
            }
        }

        character.HandleUpdate();
    }

    IEnumerator Walk()
    {
        state= NPCState.Walking;

        var oldPosition = transform.position;

        yield return character.Move(movementPattern[currentMovementPattern]);

        if(transform.position != oldPosition)
            currentMovementPattern = (currentMovementPattern += 1) % movementPattern.Count;

        state = NPCState.Idle;
    }

    public enum NPCState { Idle, Walking, dialogue}
}

