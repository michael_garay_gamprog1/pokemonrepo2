using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


[System.Serializable]
public class Pokemon
{

    [SerializeField] PokemonBase _base;
    [SerializeField] int level;

    public Pokemon(PokemonBase pBase, int plevel)
    {
        _base = pBase;
        level = plevel;

        Initialize();
    }

    public PokemonBase Base { get { return _base; } }
    public int Level { get { return level; } }

    public int Exp { get; set; }
    public int HP { get; set; }
    public List<Move> Moves { get; set; }
    public Dictionary<Stat, int> Stats { get; private set; }
    public Dictionary<Stat, int> StatBoost { get; private set; }

    public Queue<string> StatusChanges { get; private set; } = new Queue<string>();

    public Move CurrentMove { get; set; }

    public bool HpChanged { get; set; }

    public event System.Action OnStatusChanged;
    public event System.Action OnHpChanged;

    public void Initialize()
    {

        Moves = new List<Move>();
        foreach(var move in Base.LearnableMoves)
        {
            if(move.Level <= Level)
            {
                Moves.Add(new Move(move.Base));
            }

            if (Moves.Count >= PokemonBase.MaxNumofMoves)
                break;
        }

        Exp = Base.GetExpForLevel(level);

        CalculateStats();
        HP = MaxHP;

        ResetStatBoost();
    }

    void CalculateStats()
    {
        Stats = new Dictionary<Stat, int>();
        Stats.Add(Stat.Attack, Mathf.FloorToInt(Base.Attack * Level / 100f) + 5);
        Stats.Add(Stat.Defense, Mathf.FloorToInt(Base.Defense * Level / 100f) + 5);
        Stats.Add(Stat.SpAttack, Mathf.FloorToInt(Base.SpAttack * Level / 100f) + 5);
        Stats.Add(Stat.SpDefense, Mathf.FloorToInt(Base.SpDefense * Level / 100f) + 5);
        Stats.Add(Stat.Speed, Mathf.FloorToInt(Base.Speed * Level / 100f) + 5);


        MaxHP = Mathf.FloorToInt((Base.Speed * Level) / 100f + 10);
    }

    void ResetStatBoost()
    {
        StatBoost = new Dictionary<Stat, int>()
        {
            {Stat.Attack, 0 },
            {Stat.Defense, 0 },
            {Stat.SpAttack, 0 },
            {Stat.SpDefense, 0 },
            {Stat.Speed, 0 },
        };
    }

    int GetStat(Stat stat)
    {
        int statVal = Stats[stat];

        // Apply Statboost
        int boost = StatBoost[stat];
        var boostValues = new float[] { 1f, 1.5f, 2f, 2.5f, 3f, 3.5f, 4f };

        if (boost >= 0)
            statVal =  Mathf.FloorToInt (statVal* boostValues[boost]);
        else
            statVal = Mathf.FloorToInt(statVal / boostValues[-boost]);

        return statVal;
    }

    public void ApplyBoosts(List<StatBoost> statBoosts)
    {
        foreach(var statBoost in statBoosts)
        {
            var stat = statBoost.stat;
            var boost = statBoost.boost;

            StatBoost[stat] = Mathf.Clamp(StatBoost[stat] + boost, -6, 6);

            if (boost > 0)
                StatusChanges.Enqueue($"{Base.Name}'s {stat} rose!");
            else
                StatusChanges.Enqueue($"{Base.Name}'s {stat} fell!");

            Debug.Log($"{stat} has been boosted to {StatBoost[stat]}");
        }
    }

    public bool CheckLevelUp()
    {
        if(Exp > Base.GetExpForLevel(level + 1))
        {
            ++level;
            return true;
        }

        return false; 
    }

    public LearnableMove GetLearnableMoveAtCurLevel()
    {
        return Base.LearnableMoves.Where(x => x.Level == level).FirstOrDefault();
    }

    public void LearnMove(LearnableMove movetoLearn)
    {
        if (Moves.Count > PokemonBase.MaxNumofMoves)
        {
            return;
        }

        Moves.Add(new Move(movetoLearn.Base));
    }  


    public int Attack
    {
        get { return GetStat(Stat.Attack); }
        //get { return Mathf.FloorToInt((Base.Attack * Level) / 100) + 5; }
    }

    public int Defense
    {
        get { return GetStat(Stat.Defense); }
        //get { return Mathf.FloorToInt((Base.Defense * Level) / 100) + 5; }
    }

    public int SpAttack
    {
        get { return GetStat(Stat.SpAttack); }
        //get { return Mathf.FloorToInt((Base.SpAttack * Level) / 100) + 5; }
    }

    public int SpDefense
    {
        get { return GetStat(Stat.SpDefense); }
        //get { return Mathf.FloorToInt((Base.SpDefense * Level) / 100) + 5; }
    }

    public int Speed
    {
        get { return GetStat(Stat.Speed); }
        //get { return Mathf.FloorToInt((Base.Speed * Level) / 100) + 5; }
    }

    public int MaxHP { get; private set; }

    public DamageDetails TakeDamage(Move move, Pokemon attacker)
    {
        float critical = 1f;
        if (Random.value * 100f < 6.25f)
            critical = 2f;

        float type = TypeChart.GetEffectiveness(move.Base.Type, this.Base.Type1) * TypeChart.GetEffectiveness(move.Base.Type, this.Base.Type2);

            var damageDetails = new DamageDetails()
            {
                TypeEffectiveness = type,
                Critical = critical,
                Fainted = false
            };

        float attack = (move.Base.Category == MoveCategory.Special) ? attacker.SpAttack : attacker.Attack;
        float defense = (move.Base.Category == MoveCategory.Special) ? SpDefense : Defense;

        float modifiers = Random.Range(0.85f, 1f) * type * critical;
        float a =(2* attacker.Level + 10) / 250f;
        float d = a * move.Base.Power * ((float)attacker.Attack / Defense) + 2;
        int damage = Mathf.FloorToInt(d * modifiers);

        HP -= damage;
        if (HP<= 0)
        {
            HP = 0;
            damageDetails.Fainted = true;
        }

        return damageDetails;
    }

    public void IncreaseHp(int amount)
    {
        HP = Mathf.Clamp(HP + amount, 0, MaxHP);
        OnHpChanged?.Invoke(); 
    }


    public void DecreaseHp(int damage)
    {
        HP = Mathf.Clamp(HP - damage, 0, MaxHP);
        OnHpChanged?.Invoke();
    }

    public void OnBattleOver()
    {
        ResetStatBoost();
    }

    public Move GetRandomMove()
    {
        int r = Random.Range(0, Moves.Count);
        return Moves[r];
    }
}

public class DamageDetails
{
    public bool Fainted { get; set; }
    public float Critical { get; set; }
    public float TypeEffectiveness { get; set; }
}
