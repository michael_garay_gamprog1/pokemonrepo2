using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    [SerializeField] GameObject dialogueBox;
    [SerializeField] Text dialogueText;
    [SerializeField] int letterPerSecond;

    public static DialogueManager Instance { get; private set; }

    public event Action OnShowDialogue;
    public event Action OnCloseDialogue;
    Action onDialogueFinished;

    private void Awake()
    {
        Instance = this;
    }

    Dialogue dialogue1;
    int currentLine = 0;
    bool isTyping;

    public bool isShowing { get; private set; }

    public IEnumerator ShowDialogText(string text, bool waitForInput = true)
    {
        isShowing = true;
        dialogueBox.SetActive(true);

        yield return TypeDialog(text);

        if(waitForInput)
        {
            yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Z));
        }

        dialogueBox.SetActive(false);
        isShowing = false; 


    }

    public IEnumerator ShowDialogue(Dialogue dialogue, Action onFinished = null)
    {
        yield return new WaitForEndOfFrame();
        
        OnShowDialogue?.Invoke();


        isShowing = true;
        this.dialogue1 = dialogue;
        onDialogueFinished = onFinished;


        dialogueBox.SetActive(true);
        StartCoroutine(TypeDialog(dialogue.Lines[0]));
    }

    public void HandleUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Z) && isTyping == false)
        {
            ++currentLine;
            if(currentLine < dialogue1.Lines.Count)
            {
                StartCoroutine(TypeDialog(dialogue1.Lines[currentLine]));
            }
            else
            {
                currentLine = 0;
                isShowing = false;
                dialogueBox.SetActive(false);
                onDialogueFinished?.Invoke();
                OnCloseDialogue?.Invoke();
            }
        }

    }

    public IEnumerator TypeDialog(string line)
    {
        isTyping = true;
        dialogueText.text = "";
        foreach (var letter in line.ToCharArray())
        {
            dialogueText.text += letter;
            yield return new WaitForSeconds(1f / letterPerSecond);
        }
        isTyping = false;
    }
}
