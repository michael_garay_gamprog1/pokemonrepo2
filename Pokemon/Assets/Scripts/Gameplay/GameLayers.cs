using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLayers : MonoBehaviour
{
    [SerializeField] LayerMask solidsLayer;
    [SerializeField] LayerMask interactableLayer;
    [SerializeField] LayerMask longGrass;
    [SerializeField] LayerMask FOV;
    [SerializeField] LayerMask playerLayer;


    public static GameLayers i { get; set; }

    private void Awake()
    {
        i = this;
    }


    public LayerMask SolidLayer
    {
        get => solidsLayer;
    }

    public LayerMask InteractableLayer
    {
        get => interactableLayer;
    }

    public LayerMask LongGrass
    {
        get => longGrass;
    }

    public LayerMask fieldOfView
    {
        get => FOV;
    }

    public LayerMask PlayerLayer
    {
        get => playerLayer;
    }
}
